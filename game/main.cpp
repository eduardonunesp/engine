#include <engine/Engine.hpp>
#include "entities/Hero.hpp"
#include "entities/Background.hpp"
#include "systems/PlayerControlSystem.hpp"

int main(void)
{
	App app;

	auto configs = [](Configs& configs) {
		configs.title = "Game";
		configs.view_port = glm::vec2(800, 600);
		configs.window_pos = glm::vec2(0, 0);
		configs.quit = false;
		configs.close_on_quit = false;
		configs.refresh_rate = 60;
		configs.zoom_factor = 1.0f;
	};

	auto load_systems = [](SystemsRegistry& systems) {
		systems.add(new RenderSystem());
		systems.add(new PhysicsSystem());
		systems.add(new PlayerControlSystem());
	};

	auto load_actions = [](ActionsRegistry& actions) {
		actions.add("quit", KeyCode::Key::KEY_ESCAPE);
		actions.add("change_background_white", KeyCode::Key::KEY_c);
		actions.add("change_background_black", KeyCode::Key::KEY_b);
		actions.add("left", KeyCode::Key::KEY_LEFT);
		actions.add("right", KeyCode::Key::KEY_RIGHT);
		actions.add("jump", KeyCode::Key::KEY_SPACE);
	};

	auto load_entities = [](EntitiesRegistry& entities) {
		// Registering entities
		entities.add(new Background({0, 0}));
		entities.add(new Hero({76, 10}));
	};

	auto load_map = [](MapsRegistry &maps) {
		// Create map
		auto map = new Map();
		map->init("resources/Content/Tiles/BlockA0.png", 50, 50, 16);
		map->set_tile(1, 10, Tile::Type::Block);
		map->set_tile(2, 10, Tile::Type::Block);
		map->set_tile(3, 10, Tile::Type::Block);
		map->set_tile(4, 10, Tile::Type::Block);
		map->set_tile(5, 10, Tile::Type::Block);
		maps.add("map", map);
	};

	// App initialization
	app.init(
		configs,
		load_systems,
		load_actions,
		load_entities,
		load_map
	);

	return 0;
}
