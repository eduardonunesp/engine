#include "PlayerControlSystem.hpp"
#include "../components/Player.hpp"

PlayerControlSystem::~PlayerControlSystem() {}

void PlayerControlSystem::tick(class ECS::World* world, float dt)
{
	world->each<MovingObject, Animations, Player>(
		[&](ECS::Entity* ent,
			ECS::ComponentHandle<MovingObject> moving_object,
			ECS::ComponentHandle<Animations> animations,
			ECS::ComponentHandle<Player> player
			) -> void {
		if (is_key_down("quit")) {
			world->emit<WindowQuit>(WindowQuit{});
		}

		auto direction = 0;

		if (is_key_down("right")) {
			animations->hflip = true;
			direction = 1;
		}

		if (is_key_down("left")) {
			animations->hflip = false;
			direction = -1;
		}

		if (is_key_up("jump") && moving_object->on_ground) {
			moving_object->speed.y = -player->jump_speed;
			moving_object->on_ground = false;
			animations->play("jump", true);
		}

		if (moving_object->on_ground) {
			if (direction != 0) {
				animations->play("run");
			} else {
				animations->play("idle");
			}
		} else {
			animations->play("jump");
		}

		moving_object->speed.x = direction * player->speed;
	});
}
