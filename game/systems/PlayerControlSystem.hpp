#pragma once

#include <engine/Engine.hpp>

class PlayerControlSystem : public BaseInputSystem {
public:
	virtual ~PlayerControlSystem();
	virtual void tick(class ECS::World* world, float dt) override;
};
