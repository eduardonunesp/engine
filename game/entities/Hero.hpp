#pragma once

#include <engine/Engine.hpp>
#include "../components/Player.hpp"

struct Hero : WrappedEntity
{
	glm::vec2 position;

	Hero(glm::vec2 pos): position(pos) {}

	ECS::Entity* load() override
	{
		ECS::Entity* ent = create();

		ent->assign<Tag>(Tag{
			.name = "Hero",
		});

		ent->assign<Actor>(Actor{
			.type = Actor::Type::Player,
		});

		Animations anims;
		anims.addAnimation("resources/Content/Sprites/Player/Idle.png", "idle", {0, 0, 64, 64}, {0}, 1, false);
		anims.addAnimation("resources/Content/Sprites/Player/Run.png", "run", {0, 0, 64, 64}, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 10);
		anims.addAnimation("resources/Content/Sprites/Player/Jump.png", "jump", {0, 0, 64, 64}, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 10, false);
		ent->assign<Animations>(anims);

		Transform transform;
		ent->assign<Transform>(Transform{});

		MovingObject movingObject = {
			.rect = {position.x, position.y, 64, 64},
			.offset = {22, 6},
		};
		ent->assign<MovingObject>(movingObject);

		Gravity gravity = {
			.speed = {0.0f, 1000.0f},
		};
		ent->assign<Gravity>(gravity);

		Player player = {
			.speed = 300,
		};
		ent->assign<Player>(player);

		return ent;
	}
};
