#pragma once

#include <engine/Engine.hpp>
#include "../components/Player.hpp"

struct Background : WrappedEntity
{
	glm::vec2 position;

	Background(glm::vec2 pos): position(pos) {}

	ECS::Entity* load() override
	{
		ECS::Entity* ent = create();

		ent->assign<Tag>(Tag{
			.name = "Background",
		});

		Transform transform = {
			.position = position,
		};
		ent->assign<Transform>(transform);

		Sprite renderable = {
			.filename = "resources/Content/Backgrounds/Layer0_0.png",
			.z_index = -1,
		};
		ent->assign<Sprite>(renderable);

		return ent;
	}
};
