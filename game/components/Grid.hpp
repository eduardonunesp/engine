#pragma once

#include <cstdint>

struct Grid {
	// Grid size
	std::uint32_t width = 0;
	std::uint32_t height = 0;

	// Cell size
	std::uint32_t cell_sqr_size = 0;
};
