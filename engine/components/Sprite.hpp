#pragma once

#include <string>
#include <vector>
#include <map>
#include <glm/vec2.hpp>
#include "../math/Rect.hpp"
#include "../graphics/Color.hpp"

struct Sprite {
	// Path for the texture
	std::string filename = "";

	// Texture dimensions and position
	// Position only used for spritesheets not for draw destinations
	Rect rect = {0, 0, 0, 0};

	// Texture color
	Color color = 0xffffffff;

	// Texture id
	std::string id;

	// Draw index
	std::int32_t z_index = 0;
};
