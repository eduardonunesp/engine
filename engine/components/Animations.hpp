#pragma once

#include <cstdint>
#include <vector>
#include <map>
#include <string>
#include "../math/Rect.hpp"
#include "../utils/Log.hpp"
#include "Sprite.hpp"

struct Animations {
	struct Animation : Sprite {
		typedef std::vector<std::size_t> AnimationFrames;

		// Frame duration in milliseconds
		std::uint32_t fps = 1;

		// Time accumulator
		float time_acc = 0;

		// Current frame
		std::size_t anim_frame_idx = 0;

		// Stores the frame indices
		AnimationFrames frames = {0};

		// Animation should restart after end
		bool loop = true;
	};

	typedef std::map<std::string, Animation> AnimationsMap;

	// Draw index
	std::int32_t z_index = 0;

	// Flip the image horizontally
	bool hflip = false;

	// Flip the image vertically
	bool vflip = false;

	// Animation handlers
	std::string curr_animation = "";

	// Animations map
	AnimationsMap animations;

	// Plays the animation with the given name
	inline void play(const std::string& name, bool reset = false) {
		if (animations.find(name) == animations.end()) {
			return;
		}

		curr_animation = name;
		auto &animation = animations[curr_animation];
		if (reset) {
			animation.anim_frame_idx = 0;
		}
	}

	// Adds an animation to the copmponent
	inline void addAnimation(
		const std::string& filename, const std::string &name,
		const Rect rect, const std::vector<std::size_t> frames,
		std::uint32_t fps, bool loop = true
	)
	{
		Animation animation;
		animation.fps = fps;
		animation.frames = frames;
		animation.filename = filename;
		animation.rect = rect;
		animation.loop = loop;

		animations[name] = animation;

		if (curr_animation.empty())
			curr_animation = name;
	}
};
