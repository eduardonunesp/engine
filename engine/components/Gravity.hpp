#pragma once

#include <glm/vec2.hpp>

struct Gravity {
	glm::vec2 speed = {0, -1000.f};
};