#pragma once

struct Actor {
	enum class Type {
		None,
		Player,
		Enemy,
		Bullet,
		Pickup,
	};

	Type type = Type::None;
};