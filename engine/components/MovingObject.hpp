#pragma once

#include <glm/glm.hpp>
#include "../math/Rect.hpp"

struct MovingObject {
	glm::vec2 position = {0.0f, 0.0f};
	glm::vec2 speed    = {0.0f, 0.0f};
	glm::vec2 offset   = {0.0f, 0.0f};
	Rect rect          = {0.0f, 0.0f, 0, 0};
	bool on_ground     = false;
};
