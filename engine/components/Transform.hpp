#pragma once

#include <glm/vec2.hpp>

struct Transform {
	// Position of the transform
	glm::vec2 position = {0, 0};

	// Angle of the transform
	float angle = 0;

	// Scale of the transform
	float scale = 1;
};
