#pragma once

#include "../ECS.hpp"
#include "../components/Sprite.hpp"
#include "../components/Tag.hpp"
#include "../components/Transform.hpp"
#include "../components/Animations.hpp"
#include "../components/Gravity.hpp"
#include "../components/MovingObject.hpp"
#include "../components/Actor.hpp"

class EntitiesRegistry;

// WrappedEntity is a base class for all entities that are loaded into the engine
struct WrappedEntity {
	// load is a virtual function that is called when the entity is loaded into the engine
	virtual ECS::Entity* load() = 0;

	ECS::Entity* create() {
		return world->create();
	}

private:
	friend EntitiesRegistry;

	// init is a virtual function that is called when the entity is initialized
	void init(ECS::World *world)
	{
		this->world = world;
	}

	ECS::World* world;
};
