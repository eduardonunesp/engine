#include <string>
#include <cstdint>

std::string Base64Encode(unsigned char const*, std::uint32_t len);
std::string Base64Decode(std::string const& s);
