#pragma once

#include <cstdint>

template <typename T = std::int32_t>
class Size {
public:
	Size() : width(0), height(0) {}
	Size(T width, T height) : width(width), height(height) {}

	inline bool operator==(Size const& other) const {
		return width == other.width && height == other.height;
	}

	inline bool is_valid() const {
		return width > 0 && height > 0;
	}

	T width;
	T height;
};