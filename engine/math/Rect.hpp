#pragma once

#include <cstdint>
#include <glm/vec2.hpp>

struct Rect {
	enum class IntersectionSide {
		TOP = 0,
		BOTTOM = 1,
		LEFT = 2,
		RIGHT = 3,
		NONE = 4
	};

	float x = 0.0f;
	float y = 0.0f;
	std::int32_t w = 0;
	std::int32_t h = 0;

	Rect() : x(0), y(0), w(0), h(0) {}
	Rect(float x, float y, std::int32_t w, std::int32_t h) : x(x), y(y), w(w), h(h) {}

	void position(float x, float y) {
		this->x = x;
		this->y = y;
	}

	void position(const glm::vec2& position) {
		this->x = position.x;
		this->y = position.y;
	}

	void move(const glm::vec2& position) {
		this->x += position.x;
		this->y += position.y;
	}

	glm::vec2 position() const {
		return glm::vec2(x, y);
	}

	bool is_zero() const {
		return w == 0 || h == 0;
	}

	void operator=(const glm::vec2 &position) {
		this->x = position.x;
		this->y = position.y;
	}

	void operator+=(const glm::vec2 &position) {
		this->x += position.x;
		this->y += position.y;
	}

	bool operator==(const Rect &other) const {
		return x == other.x && y == other.y && w == other.w && h == other.h;
	}

	bool operator!=(const Rect &other) const {
		return !(*this == other);
	}

	Rect operator+(const Rect &other) const {
		return Rect(x + other.x, y + other.y, w + other.w, h + other.h);
	}

	Rect operator+(const glm::vec2 &position) const {
		return Rect(x + position.x, y + position.y, w, h);
	}

	Rect operator-(const Rect &other) const {
		return Rect(x - other.x, y - other.y, w - other.w, h - other.h);
	}

	bool contains(float x, float y) const {
		return x >= this->x && x <= this->x + w && y >= this->y && y <= this->y + h;
	}

	bool intersects(const Rect &other) const {
		return !(x > other.x + other.w || y > other.y + other.h || x + w < other.x || y + h < other.y);
	}

	Rect::IntersectionSide side_of_intersection(const Rect &other) const {
		if (x > other.x + other.w) {
			return Rect::IntersectionSide::LEFT;
		} else if (y > other.y + other.h) {
			return Rect::IntersectionSide::TOP;
		} else if (x + w < other.x) {
			return Rect::IntersectionSide::RIGHT;
		} else if (y + h < other.y) {
			return Rect::IntersectionSide::BOTTOM;
		} else {
			return Rect::IntersectionSide::NONE;
		}
	}
};
