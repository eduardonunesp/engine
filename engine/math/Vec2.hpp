#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include <glm/glm.hpp>

struct Vec2 {
	static Vec2 zero() { return Vec2(0, 0); }
	static Vec2 left() { return Vec2(-1, 0); }
	static Vec2 right() { return Vec2(1, 0); }
	static Vec2 up() { return Vec2(0, -1); }
	static Vec2 down() { return Vec2(0, 1); }

	Vec2(): x(0.0f), y(0.0f) {}
	Vec2(float x, float y): x(x), y(y) {}

	Vec2 operator+(Vec2 const& v)
	{
		return Vec2(
			x + v.x,
			y + v.y
		);
	}

	Vec2 operator+=(Vec2 const& v)
	{
		x += v.x;
		y += v.y;

		return *this;
	}

	Vec2 operator-(Vec2 const& v)
	{
		return Vec2(
			x - v.x,
			y - v.y);
	}

	Vec2 operator-=(Vec2 const& v)
	{
		x -= v.x;
		y -= v.y;

		return *this;
	}


	Vec2 operator*(Vec2 const& rhs) const
	{
		return Vec2(
			x * rhs.x,
			y * rhs.y
		);
	}

	Vec2 operator*=(Vec2 const& rhs)
	{
		x *= rhs.x;
		y *= rhs.y;
		return *this;
	}

	Vec2 operator*(float rhs) const
	{
		return Vec2(
			x * rhs,
			y * rhs
		);
	}

	Vec2 operator*=(float rhs)
	{
		x *= rhs;
		y *= rhs;
		return *this;
	}

	float magnitude() const
	{
		return std::sqrt(pow(x, 2) + pow(y, 2));
	}

	// / Normalizes the vector
	void normalize()
	{
		float l = magnitude();
		if (l > 0) {
			(*this) *= 1.0f / l;
		}
	}

	/// Returns the dot product between two vectors
	/// @param other the other vector
	float dot(const Vec2 &other) const
	{
		return (x * other.x) + (y * other.y);
	}

	/// Returns the cross product between two vectors
	/// @param other the other vector
	float cross(const Vec2 &other)
	{
		return (x * other.y) + (y * other.x);
	}

	/// Returns the angle between two vectors in radians
	/// Multiple for (180/M_PI) to return the angle in degrees
	/// @param other the other vector
	float angle(const Vec2 &other) const
	{
		auto mag = magnitude();
		auto other_mag = other.magnitude();
		auto dot_product = dot(other);
		return std::acos(dot_product / (mag * other_mag));
	}

	Vec2 slerp(const Vec2 &other, float t)
	{
		auto self = glm::vec2(x, y);
		auto other_vec = glm::vec2(other.x, other.y);
		auto result = glm::mix(self, other_vec, t);
		return Vec2(result.x, result.y);
	}

	friend std::ostream& operator<<(std::ostream& os, const Vec2 &vec)
	{
		std::string s(vec);
		os << " " << s << " ";
		return os;
	}

	operator std::string() const
	{
		std::ostringstream ss;
		ss << "Vec2(" << x << ", " << y << ")";
		return ss.str();
	}

	float x, y;
};
