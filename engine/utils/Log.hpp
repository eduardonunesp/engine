#pragma once

#include <iostream>
#include "Format.hpp"

class Log
{
public:
	enum Level
	{
		Error,
		Debug,
		Warning
	};
	static std::ostream &GetStream() { return std::cout; }
	static bool IsLevelActive(Level l) { return true; }
};

#ifdef _DEBUG
#define DBG(M)                                          \
	do                                                  \
	{                                                   \
		if (Log::IsLevelActive(Log::Debug))             \
			(Log::GetStream() << "DBG: " << M << "\n"); \
	} while (false)

#else
#define DBG(M)
#endif

#define WRN(M)                                          \
	do                                                  \
	{                                                   \
		if (Log::IsLevelActive(Log::Warning))           \
			(Log::GetStream() << "WRN: " << M << "\n"); \
	} while (false)

#define ERR(M)                                          \
	do                                                  \
	{                                                   \
		if (Log::IsLevelActive(Log::Error))             \
			(Log::GetStream() << "ERR: " << M << "\n"); \
	} while (false)
