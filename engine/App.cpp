
#include <bitset>
#include <cassert>
#include <chrono>
#include <iostream>
#include <thread>
#include "App.hpp"
#include "events/WindowQuit.hpp"
#include "events/AppReady.hpp"
#include "events/KeyDown.hpp"
#include "events/KeyUp.hpp"
#include "events/KeyOnce.hpp"
#include "events/LoadMap.hpp"
#include "utils/Log.hpp"

void App::init(
		ConfigureCallback configure,
		LoadSystemsCallback load_systems,
		LoadActionsCallback load_actions,
		LoadEntitiesCallback load_entities,
		LoadMapCallback load_map
)
{
	configure(configs);

	window.init(configs.title, configs.view_port, configs.window_pos);

	timers.init();

	world = ECS::World::createWorld();
	assert(world != nullptr && "Window failed to initialize world");

	windowQuitSubcriber.callback = [&]() {
		configs.quit = true;
	};
	world->subscribe<WindowQuit>(&windowQuitSubcriber);

	registries.entities.init(world);
	registries.systems.init(world);
	registries.maps.init(world);

	events.on_quit = [&]() {
		DBG("Received quit event");
		configs.quit = true;
	};

	events.on_keydown = [&](const KeyCode::Key key_code) {
		world->emit(KeyDown(key_code));
	};

	events.on_keyup = [&](const KeyCode::Key key_code) {
		world->emit(KeyUp(key_code));
	};

	events.on_window_resize = [&](const int width, const int height) {
		configs.view_port.x = width;
		configs.view_port.y = height;
		window.resize(width, height);
	};

	load_systems(registries.systems);

	world->emit(AppReady{
		.configs = &configs,
		.window = &window,
		.actions = &registries.actions,
		.entities = &registries.entities,
		.maps = &registries.maps
	});

	load_actions(registries.actions);
	load_entities(registries.entities);
	load_map(registries.maps);

	loop();

	window.cleanup();
}

void App::cleanup()
{
	if (world != nullptr) {
		world->destroyWorld();
	}

	DBG("Cleanup done");
}

void App::loop()
{
	if (!world) {
		ERR("Init function not called, create nothing");
		return;
	}

	while (!configs.quit) {
		events.process_events();
		float dt = timers.calculate();
		world->tick(0.1f);
	}
}
