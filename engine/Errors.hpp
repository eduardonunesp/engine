#pragma once

struct Error {
	enum class Kind {
		Timeout,
		Invalid,
		TooLong,
		NullPtr,
	};

	Error(Kind kind, std::string text): kind(kind), text(text) {};

	Kind kind;
	std::string text;
};