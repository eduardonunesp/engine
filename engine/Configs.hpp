#pragma once

#include <string>
#include <glm/vec2.hpp>

// App configs with defualt values
struct Configs {
	std::string title = "Untitled Game";
	glm::vec2 view_port = glm::vec2(800, 600);
	glm::vec2 window_pos = glm::vec2(0, 0);
	bool quit = false;
	bool close_on_quit = false;
	int refresh_rate = 60;
	float zoom_factor = 1.0f;
};
