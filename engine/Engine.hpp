#pragma once

#include "ECS.hpp"
#include "App.hpp"

#include "components/Transform.hpp"
#include "components/Sprite.hpp"
#include "components/Tag.hpp"
#include "components/Animations.hpp"
#include "components/Gravity.hpp"
#include "components/MovingObject.hpp"
#include "components/Actor.hpp"

#include "core/SDLWrappers.hpp"
#include "core/ActionsRegistry.hpp"
#include "core/PlatformOS.hpp"
#include "core/SystemsRegistry.hpp"
#include "core/MapsRegistry.hpp"

#include "entities/WrappedEntity.hpp"

#include "events/AppReady.hpp"
#include "events/KeyDown.hpp"
#include "events/KeyOnce.hpp"
#include "events/KeyUp.hpp"
#include "events/LoadShapes.hpp"
#include "events/LoadTexture.hpp"
#include "events/WindowQuit.hpp"
#include "events/WindowReady.hpp"
#include "events/WindowResize.hpp"
#include "events/WindowWillQuit.hpp"

#include "math/Base64.hpp"

#include "systems/BaseInputSystem.hpp"
#include "systems/RenderSystem.hpp"
#include "systems/PhysicsSystem.hpp"

#include "utils/Format.hpp"
#include "utils/Log.hpp"
#include "utils/Result.hpp"

#include "graphics/Color.hpp"

#include "core/tilemap/Map.hpp"
#include "core/tilemap/Tile.hpp"
