#pragma once

class Configs;
class Window;
class ActionsRegistry;
class EntitiesRegistry;
class MapsRegistry;

struct AppReady {
	Configs* configs;
	Window* window;
	ActionsRegistry* actions;
	EntitiesRegistry* entities;
	MapsRegistry* maps;
};
