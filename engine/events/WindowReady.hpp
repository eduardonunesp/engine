#pragma once

#include "../core/SDLWrappers.hpp"

struct App;

struct WindowReady {
	// Window information
	Window *window;
};
