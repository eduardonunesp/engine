#pragma once

#include <cstdint>

struct WindowResize {
	// Window new width and height
	std::int32_t width;
	std::int32_t height;
};
