#pragma once

#include <cstdint>

class Color {
public:
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
	Color() : r(0), g(0), b(0), a(0) {}
	Color(std::uint32_t hex) {
		r = (hex >> 24) & 0xFF;
		g = (hex >> 16) & 0xFF;
		b = (hex >> 8) & 0xFF;
		a = hex & 0xFF;
	}

	std::uint8_t r;
	std::uint8_t g;
	std::uint8_t b;
	std::uint8_t a;
};
