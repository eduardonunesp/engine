#pragma once

#include "App.hpp"

// Extend this class to create the initialization for your app
// The setup function should be overridden to initialize your app correctly
class Startup {
// public:
// 	void init(const App& app) {
// 		this.app = app;
// 		setup(app);
// 	}

// 	virtual void setup(const App& app) = 0;

// private:
// 	friend App;

// 	App &app;

// 	void run()
// 	{
// 		// App loop and cleanup
// 		app.loop();
// 		app.cleanup();
// 	}
};
