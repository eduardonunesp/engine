#include "BaseInputSystem.hpp"
#include "../App.hpp"
#include "../components/Sprite.hpp"
#include "../utils/Log.hpp"

void BaseInputSystem::tick(class ECS::World* world, float dt) {}

void BaseInputSystem::configure(class ECS::World* world)
{
	world->subscribe<KeyDown>(this);
	world->subscribe<KeyUp>(this);
	world->subscribe<KeyOnce>(this);
	world->subscribe<AppReady>(this);
}

void BaseInputSystem::unconfigure(class ECS::World* world)
{
	world->unsubscribeAll(this);
}

bool BaseInputSystem::is_key_down(KeyCode::Key key_code)
{
	return keysDown[key_code];
}

bool BaseInputSystem::is_key_down(const std::string& action)
{
	return is_key_down(actions->get(action));
}

bool BaseInputSystem::is_key_up(KeyCode::Key key_code)
{
	if (keysUp[key_code]) {
		keysUp[key_code] = false;
		return true;
	}

	return false;
}

bool BaseInputSystem::is_key_up(const std::string& action)
{
	return is_key_up(actions->get(action));
}

bool BaseInputSystem::is_key_pressed(KeyCode::Key key_code)
{
	if (keysOnce[key_code]) {
		keysOnce[key_code] = false;
		return true;
	}

	return false;
}

bool BaseInputSystem::is_key_pressed(const std::string& action)
{
	return is_key_pressed(actions->get(action));
}

void BaseInputSystem::receive(class ECS::World *world, const KeyDown &event)
{
	keysDown[event.key] = true;
	keysUp[event.key] = false;
}

void BaseInputSystem::receive(class ECS::World *world, const KeyUp &event)
{
	keysDown[event.key] = false;
	keysUp[event.key] = true;
	keysOnce[event.key] = false;
}

void BaseInputSystem::receive(class ECS::World *world, const KeyOnce &event)
{
	keysOnce[event.key] = true;
}

void BaseInputSystem::receive(class ECS::World *world, const AppReady &event)
{
	actions = event.actions;
}
