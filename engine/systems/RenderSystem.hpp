#pragma once

#include <map>
#include "../ECS.hpp"
#include "../core/MapsRegistry.hpp"
#include "../events/AppReady.hpp"
#include "../events/LoadMap.hpp"
#include "../core/SDLWrappers.hpp"
#include "../components/Sprite.hpp"
#include "../components/Animations.hpp"

class RenderSystem : public ECS::EntitySystem,
	public ECS::EventSubscriber<AppReady>,
	public ECS::EventSubscriber<ECS::Events::OnComponentAssigned<Sprite>>,
	public ECS::EventSubscriber<ECS::Events::OnComponentAssigned<Animations>>,
	public ECS::EventSubscriber<LoadMap>
	{
public:
	virtual ~RenderSystem() {};

	virtual void configure(class ECS::World* world) override;
	virtual void unconfigure(class ECS::World* world) override;
	virtual void tick(class ECS::World* world, float dt) override;

	virtual void receive(class ECS::World* world, const AppReady& event) override;
	virtual void receive(class ECS::World* world, const ECS::Events::OnComponentAssigned<Sprite>& event) override;
	virtual void receive(class ECS::World* world, const ECS::Events::OnComponentAssigned<Animations>& event) override;
	virtual void receive(class ECS::World* world, const LoadMap& event) override;


private:
	Window* window;
	EntitiesRegistry *entities;
	MapsRegistry *maps;
};
