#pragma once

#include <map>
#include <string>

#include "../core/SDLWrappers.hpp"
#include "../ECS.hpp"
#include "../events/KeyDown.hpp"
#include "../events/KeyUp.hpp"
#include "../events/KeyOnce.hpp"
#include "../events/AppReady.hpp"

class ActionsRegistry;

class BaseInputSystem : public ECS::EntitySystem,
	public ECS::EventSubscriber<AppReady>,
	public ECS::EventSubscriber<KeyDown>,
	public ECS::EventSubscriber<KeyUp>,
	public ECS::EventSubscriber<KeyOnce> {
public:
	typedef std::map<KeyCode::Key, bool> KeyMap;

	virtual ~BaseInputSystem() {};
	virtual void tick(class ECS::World* world, float dt) override;
	virtual void configure(class ECS::World* world) override;
	virtual void unconfigure(class ECS::World* world) override;

	virtual void receive(class ECS::World *world, const AppReady &event) override;
	virtual void receive(class ECS::World *world, const KeyDown &event) override;
	virtual void receive(class ECS::World *world, const KeyUp &event) override;
	virtual void receive(class ECS::World *world, const KeyOnce &event) override;

protected:
	bool is_key_down(KeyCode::Key key_code);
	bool is_key_down(const std::string& action);

	bool is_key_up(KeyCode::Key key_code);
	bool is_key_up(const std::string& action);

	bool is_key_pressed(KeyCode::Key key_code);
	bool is_key_pressed(const std::string& action);

	ActionsRegistry *actions;

private:
	KeyMap keysDown;
	KeyMap keysUp;
	KeyMap keysOnce;
};
