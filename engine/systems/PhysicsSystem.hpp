#pragma once

#include "../ECS.hpp"
#include "../events/LoadMap.hpp"

class Map;

class PhysicsSystem : public ECS::EntitySystem,
	public ECS::EventSubscriber<LoadMap>
{
public:
	virtual ~PhysicsSystem() {}

	virtual void configure(class ECS::World* world) override;
	virtual void unconfigure(class ECS::World* world) override;
	virtual void tick(class ECS::World* world, float dt) override;

	virtual void receive(class ECS::World* world, const LoadMap& event) override;

protected:
	Map *map;
};
