#include <cassert>

#include "PhysicsSystem.hpp"
#include "../App.hpp"
#include "../components/Transform.hpp"
#include "../components/MovingObject.hpp"
#include "../components/Tag.hpp"
#include "../components/Gravity.hpp"
#include "../utils/Log.hpp"

#define FALL_LIMIT 200.f
#define FALL_LIMIT_SPEED 500.f

void PhysicsSystem::tick(class ECS::World* world, float dt)
{
	// No map, no physics
	if (map == nullptr)
		return;

	world->each<Transform, MovingObject>([&](
		ECS::Entity* ent,
		ECS::ComponentHandle<Transform> transform,
		ECS::ComponentHandle<MovingObject> moving_object) -> void {
		// moving_object->old_aabb = transform->position;
		// moving_object->old_speed = moving_object->speed;
		// moving_object->was_on_ground = moving_object->on_ground;

		// moving_object->aabb.move(moving_object->speed * dt);

		// DBG("moving_object->aabb: " << moving_object->aabb.x << " " << moving_object->aabb.y << " " << moving_object->aabb.w << " " << moving_object->aabb.h);

		// if (ent->has<Gravity>()) {
		// 	auto gravity = ent->get<Gravity>();
		// 	moving_object->speed += gravity->speed * dt;
		// 	moving_object->speed.y = std::min(moving_object->speed.y, FALL_LIMIT_SPEED);

		// 	float ground_y = 0;
		// 	if (moving_object->speed.y >= 0 && map->has_ground(
		// 		&moving_object.get(),
		// 		moving_object->old_aabb.position(),
		// 		moving_object->aabb.position(),
		// 		ground_y)) {
		// 		moving_object->aabb.y = ground_y - (moving_object->aabb.w + moving_object->offset.y);
		// 		moving_object->speed.y = 0;
		// 		moving_object->on_ground = true;
		// 	} else {
		// 		moving_object->on_ground = false;
		// 	}
		// }

		// moving_object->aabb = moving_object->aabb + moving_object->offset;

		// transform->position.x = round(moving_object->aabb.x) * dt;
		// transform->position.y = round(moving_object->aabb.y) * dt;
	});
}

void PhysicsSystem::configure(class ECS::World* world)
{
	world->subscribe<LoadMap>(this);
}

void PhysicsSystem::unconfigure(class ECS::World* world)
{
	world->unsubscribeAll(this);
}

void PhysicsSystem::receive(class ECS::World* world, const LoadMap& event)
{
	map = event.map;
}