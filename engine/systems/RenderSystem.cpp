#include <cassert>
#include <glm/vec2.hpp>
#include "RenderSystem.hpp"
#include "../App.hpp"
#include "../components/Transform.hpp"
#include "../components/Sprite.hpp"
#include "../components/MovingObject.hpp"
#include "../components/Tag.hpp"
#include "../graphics/Color.hpp"
#include "../utils/Log.hpp"

void RenderSystem::tick(class ECS::World* world, float dt)
{
	window->clear();
	float zoom = 2.0f;

	for (auto entity_layer : entities->entities_by_layer) {
		for (auto entity : entity_layer.second) {
			if (entity->has<Transform, Sprite, Tag>()) {
				auto transform = entity->get<Transform>();
				auto renderable = entity->get<Sprite>();

				Rect src = {0, 0, renderable->rect.w, renderable->rect.h};

				Rect dst = {
					transform->position.x * zoom,
					transform->position.y * zoom,
					static_cast<int>(renderable->rect.w * zoom),
					static_cast<int>(renderable->rect.h * zoom)
				};

				window->draw(renderable->filename, src, dst);
			}

			if (entity->has<Transform, Animations, Tag>()) {
				auto transform = entity->get<Transform>();
				auto animations = entity->get<Animations>();
				auto &animation = animations->animations[animations->curr_animation];

				animation.time_acc += dt;
				if (animation.time_acc > 1.0f / animation.fps) {
					if (++animation.anim_frame_idx > animation.frames.size() - 1) {
						if (animation.loop) {
							animation.anim_frame_idx = 0;
						} else {
							animation.anim_frame_idx = animation.frames.size() - 1;
						}
					}
					animation.time_acc = 0;
				}

				auto frame = animation.frames[animation.anim_frame_idx];

				DBG("frame: " << frame);

				Rect src = {
					animation.rect.x * frame,
					0,
					static_cast<int>(animation.rect.w),
					static_cast<int>(animation.rect.h)
				};

				Rect dst = {
					transform->position.x * zoom,
					transform->position.y * zoom,
					static_cast<int>(animation.rect.w * zoom),
					static_cast<int>(animation.rect.h * zoom)
				};

				int flip_flags = SDL_FLIP_NONE;
				if (animations->hflip)
					flip_flags |= SDL_FLIP_HORIZONTAL;
				if (animations->vflip)
					flip_flags |= SDL_FLIP_VERTICAL;


				window->draw(animation.filename, src, dst);
			}
		}
	}

	if (maps != nullptr)
		maps->render();

	window->present();
}

void RenderSystem::configure(class ECS::World* world)
{
	world->subscribe<AppReady>(this);
	world->subscribe <ECS::Events::OnComponentAssigned<Sprite>>(this);
	world->subscribe <ECS::Events::OnComponentAssigned<Animations>>(this);
}

void RenderSystem::unconfigure(class ECS::World* world)
{
	world->unsubscribeAll(this);
}

void RenderSystem::receive(class ECS::World* world, const AppReady& event)
{
	window = event.window;
	entities = event.entities;
	window->set_clear_color(0x000000ff);
}

void RenderSystem::receive(class ECS::World* world, const ECS::Events::OnComponentAssigned<Sprite>& event)
{
	auto renderable = event.entity->get<Sprite>();
	if (renderable) {
		DBG("Renderable assigned to entity " << renderable->filename);
		auto texture = window->load_texture(renderable->filename);

		if (renderable->rect.is_zero()) {
			renderable->rect = texture->get_rect();
		}

		if (renderable->id.empty()) {
			renderable->id = renderable->filename;
		}
	}
};

void RenderSystem::receive(class ECS::World* world, const ECS::Events::OnComponentAssigned<Animations>& event)
{
	auto animations = event.entity->get<Animations>();
	if (animations) {
		for (auto& anim : animations->animations) {
			DBG("Animation assigned to entity " << anim.first);
			auto texture = window->load_texture(anim.second.filename);

			if (anim.second.id.empty()) {
				anim.second.id = anim.second.filename;
			}
		}
	};
};

void RenderSystem::receive(class ECS::World* world, const LoadMap& event)
{
	DBG("Map texture loaded: " << event.map->get_filename());
	window->load_texture(event.map->get_filename());
}