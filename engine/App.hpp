#pragma once

#include <functional>
#include <string>
#include "ECS.hpp"
#include "Configs.hpp"
#include "core/tilemap/Map.hpp"
#include "core/SDLWrappers.hpp"
#include "core/EntitiesRegistry.hpp"
#include "core/SystemsRegistry.hpp"
#include "core/ActionsRegistry.hpp"
#include "core/MapsRegistry.hpp"
#include "core/subscribers/WindowQuitSubscriber.hpp"
#include "entities/WrappedEntity.hpp"

class App
{
public:
	~App()
	{
		cleanup();
	}

	// App registries
	struct Registries {
		EntitiesRegistry entities;
		SystemsRegistry systems;
		ActionsRegistry actions;
		MapsRegistry maps;
	};

	typedef std::function<void(Configs &configs)> ConfigureCallback;
	typedef std::function<void(SystemsRegistry &systems)> LoadSystemsCallback;
	typedef std::function<void(EntitiesRegistry &entities)> LoadEntitiesCallback;
	typedef std::function<void(ActionsRegistry &actions)> LoadActionsCallback;
	typedef std::function<void(MapsRegistry &maps)> LoadMapCallback;

	// Initializes the app and creates the window
	void init(
		ConfigureCallback configure,
		LoadSystemsCallback load_systems,
		LoadActionsCallback load_actions,
		LoadEntitiesCallback load_entities,
		LoadMapCallback load_map
	);

	// Register an entity to the app
	inline void register_entity(WrappedEntity* ent)
	{
		registries.entities.add(ent);
	}

	// Register a system to the app
	inline void register_system(ECS::EntitySystem *system)
	{
		registries.systems.add(system);
	}

	// Get the zoom factor for the app
	inline const float get_zoom_factor() const
	{
		return configs.zoom_factor;
	}

private:
	Configs configs;

	Window window;

	Timers timers;

	Events events;

	Registries registries;

	WindowQuitSubscriber windowQuitSubcriber;

	ECS::World *world = nullptr;

	// Cleanup and close the app
	void cleanup();

	// Run the app
	void loop();
};
