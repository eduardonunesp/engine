#pragma once

#include <map>
#include <string>
#include "../core/SDLWrappers.hpp"

class ActionsRegistry {
public:
	typedef std::map<std::string, KeyCode::Key> ActionsMap;

	// Add actions to the app
	void add(const std::string& name, KeyCode::Key key)
	{
		actions[name] = key;
	}

	KeyCode::Key get(const std::string& name)
	{
		if (actions.find(name) == actions.end()) {
			return KeyCode::Key::KEY_UNKNOWN;
		}

		return actions[name];
	}

private:
	ActionsMap actions;
};