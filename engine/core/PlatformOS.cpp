#include "PlatformOS.hpp"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#ifdef _WIN64
PlatformOS::Platform curr_platform_os = PlatformOS::Platform::WIN_X64;
#else
PlatformOS::Platform curr_platform_os = PlatformOS::Platform::WIN_386;
#endif
#elif __APPLE__
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
PlatformOS::Platform curr_platform_os = PlatformOS::Platform::IOS_SIM;
#elif TARGET_OS_IPHONE
PlatformOS::Platform curr_platform_os = PlatformOS::Platform::IOS_IPHONE;
#elif TARGET_OS_MAC
PlatformOS::Platform  PlatformOS::curr_platform_os = = PlatformOS::MACOS;
#else
#   error "Unknown Apple platform"
#endif
#elif __linux__
PlatformOS::Platform  PlatformOS::curr_platform_os = PlatformOS::Platform::LINUX;
#else
#	error "Unknown compiler"
#endif