#include <cassert>
#include <glm/glm.hpp>
#include "Map.hpp"
#include "../../App.hpp"
#include "../../components/MovingObject.hpp"
#include "../../utils/Log.hpp"
#include "../../graphics/Color.hpp"

constexpr glm::vec2 right = glm::vec2(1.0f, 0.0f);
constexpr glm::vec2 left = glm::vec2(-1.0f, 0.0f);
constexpr glm::vec2 up = glm::vec2(0.0f, -1.0f);
constexpr glm::vec2 down = glm::vec2(0.0f, 1.0f);

bool Map::has_ground(MovingObject *moving_object, glm::vec2 old_position, glm::vec2 new_position, float &ground_y)
{
	auto old_bottom_left = (old_position + glm::vec2(0.0f, moving_object->rect.w - moving_object->offset.y));
	auto new_bottom_left = (new_position + glm::vec2(0.0f, moving_object->rect.w - moving_object->offset.y));

	std::int32_t end_y = get_map_tile_y_at_point(new_bottom_left.y);
	std::int32_t beg_y = std::max(get_map_tile_y_at_point(old_bottom_left.y), end_y);
	std::int32_t dist = std::max(std::abs(end_y - beg_y), 1);

	std::int32_t tile_index_x = 0;

	for (int tile_index_y = beg_y; tile_index_y >= end_y; --tile_index_y) {
		auto bottom_left = glm::mix(new_bottom_left, old_bottom_left, (float)std::abs(end_y - tile_index_y) / dist);
		auto bottom_right = glm::vec2(bottom_left.x + moving_object->rect.w, bottom_left.y);

		for (auto checked_tile = bottom_left; ; checked_tile.x += tile_size) {
			checked_tile.x = std::min(checked_tile.x, bottom_right.x);

			tile_index_x = get_map_tile_x_at_point(checked_tile.x);
			ground_y = (float)tile_index_y * tile_size + position.y;

			if (is_obstacle(tile_index_x, tile_index_y)) {
				return true;
			}

			if (checked_tile.x >= bottom_right.x)
				break;
		}
	};

	return false;
}

void Map::render()
{
	// for (std::int32_t tile_index_y = 0; tile_index_y < height; ++tile_index_y) {
	// 	for (std::int32_t tile_index_x = 0; tile_index_x < width; ++tile_index_x) {
	// 		auto tile_type = get_type(tile_index_x, tile_index_y);
	// 		if (tile_type == Tile::Type::Block) {
	// 			auto tile_position = glm::vec2(tile_index_x * tile_size, tile_index_y * tile_size) + position;
	// 			SDL_Rect dst = {
	// 				static_cast<int>(tile_position.x * zoom),
	// 				static_cast<int>(tile_position.y * zoom),
	// 				static_cast<int>((tile_size * zoom) + 1),
	// 				static_cast<int>((tile_size * zoom) + 1),
	// 			};

	// 			// SDL_RenderCopy(renderer, app->get_texture(filename).unwrap(), nullptr, &dst);

	// 			// Color color = 0xff0000ff;
	// 			// SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	// 			// SDL_RenderDrawRect(renderer, &dst);
	// 		}
	// 	}
	// }
}
