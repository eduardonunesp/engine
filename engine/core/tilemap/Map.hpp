#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <glm/vec2.hpp>
#include "Tile.hpp"
#include "../SDLWrappers.hpp"

class Window;
class MovingObject;
class TexturesRegistry;

class Map {
public:
	inline void init(const std::string &filename, uint32_t width, uint32_t height, uint32_t tile_size)
	{
		this->filename = filename;
		this->width = width;
		this->height = height;
		this->tile_size = tile_size;
		tiles.resize(width * height);
	}

	inline void set_tile(uint32_t x, uint32_t y, Tile::Type type)
	{
		tiles[y * width + x] = {
			.type = type,
		};
	}

	inline Tile& get(std::int32_t x, std::int32_t y)
	{
		return tiles[y * width + x];
	}

	inline bool is_obstacle(std::int32_t x, std::int32_t y)
	{
		return get(x, y).type == Tile::Type::Block;
	}

	inline Tile::Type get_type(std::int32_t x, std::int32_t y)
	{
		if (x < 0 || x >= width || y < 0 || y >= height) {
			return Tile::Type::Block;
		}

		return get(x, y).type;
	}

	inline glm::ivec2 get_map_tile_at_point(glm::vec2 point)
	{
		return glm::ivec2((std::int32_t)((point.x - position.x + tile_size) / (float)(tile_size)),
					(std::int32_t)((point.y - position.y + tile_size) / (float)(tile_size)));
	}

	inline std::int32_t get_map_tile_y_at_point(float y)
	{
		return (std::int32_t)((y - position.y + tile_size) / (float)(tile_size));
	}

	inline std::int32_t get_map_tile_x_at_point(float x)
	{
		return (std::int32_t)((x - position.x + tile_size) / (float)(tile_size));
	}

	inline glm::vec2 get_map_tile_position(int tile_index_x, int tile_index_y)
	{
		return glm::vec2(
				(float)(tile_index_x * tile_size) + position.x,
				(float)(tile_index_y * tile_size) + position.y
		);
	}

	inline glm::vec2 get_map_tile_at_position(glm::ivec2 tile_coords)
	{
		return glm::vec2(
			(float)(tile_coords.x * tile_size) + position.x,
			(float)(tile_coords.y * tile_size) + position.y
		);
	}

	const std::string& get_filename() {
		return filename;
	}

	inline void set_window(Window* window) {
		if (window) return;
		this->window = window;
	}

	bool has_ground(MovingObject *moving_object, glm::vec2 old_position, glm::vec2 new_position, float &ground_y);

	void render();

protected:
	Window *window;
	std::string filename;
	std::vector<Tile> tiles;
	std::int32_t width = 0;
	std::int32_t height = 0;
	std::uint8_t tile_size = 16;
	glm::vec2 position = {0.0f, 0.0f};

	// Draw index
	std::int8_t z_index = 0;
};
