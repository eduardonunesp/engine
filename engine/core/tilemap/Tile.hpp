#pragma once

struct Tile {
	enum class Type {
		Empty,
		Block,
		OneWay,
	};

	Type type = Type::Empty;
};