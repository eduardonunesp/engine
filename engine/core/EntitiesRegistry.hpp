#pragma once

#include <map>
#include <list>
#include <cassert>
#include "../ECS.hpp"
#include "../components/Sprite.hpp"
#include "../components/Animations.hpp"
#include "../components/Tag.hpp"
#include "../entities/WrappedEntity.hpp"

class RenderSystem;
class EntitiesRegistry {
public:
	typedef std::map<std::int16_t, std::list<ECS::Entity*>> EntityMap;

	void init(ECS::World* world) {
		assert(world != nullptr);
		this->world = world;
	}

	void add(WrappedEntity* entity)
	{
		entity->init(world);

		auto ecs_entity = entity->load();

		auto tag = ecs_entity->get<Tag>();
		if (tag) {
			DBG("Adding entity with tag " << tag->name);
		}

		auto renderable = ecs_entity->get<Sprite>();
		if (renderable) {
			DBG("Renderable assigned to entity " << renderable->filename << " with index: " << renderable->z_index);
			entities_by_layer[renderable->z_index].push_back(ecs_entity);
		}

		auto animations = ecs_entity->get<Animations>();
		if (animations) {
			DBG("Animation assigned to entity index: " << animations->z_index);
			entities_by_layer[animations->z_index].push_back(ecs_entity);
		}
	}

private:
	friend RenderSystem;

	ECS::World *world;
	EntitiesRegistry::EntityMap entities_by_layer;
};
