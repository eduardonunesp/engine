#pragma once

#include <functional>
#include "../../ECS.hpp"
#include "../../events/LoadMap.hpp"

struct LoadMapSubscriber : public ECS::EventSubscriber<LoadMap>
{
	typedef std::function<void(Map*)> LoadMapCallback;

	virtual void receive(class ECS::World *world, const LoadMap &event) override
	{
		if (callback)
			callback(event.map);
	}

	LoadMapCallback callback;
};
