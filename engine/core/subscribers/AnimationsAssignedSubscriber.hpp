#pragma once

#include <functional>
#include "../../ECS.hpp"
#include "../../components/Animations.hpp"

struct AnimationsAssignedSubscriber : public ECS::EventSubscriber<ECS::Events::OnComponentAssigned<Animations>>
{
	typedef std::function<void(ECS::ComponentHandle<Animations>)> AnimationsAssignedCallback;

	virtual void receive(class ECS::World *world, const ECS::Events::OnComponentAssigned<Animations> &event) override
	{
		auto animations = event.entity->get<Animations>();
		if (animations && callback)
			callback(animations);
	}

	AnimationsAssignedCallback callback;
};
