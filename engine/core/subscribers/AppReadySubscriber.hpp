#pragma once

#include <functional>
#include "../../ECS.hpp"
#include "../../events/AppReady.hpp"

struct AppReadySubscriber : public ECS::EventSubscriber<AppReady>
{
	typedef std::function<void(AppReady)> AppReadyCallback;

	virtual void receive(class ECS::World *world, const AppReady &event) override
	{
		if (callback)
			callback(event);
	}

	AppReadyCallback callback;
};
