#pragma once

#include <functional>
#include "../../ECS.hpp"
#include "../../components/Sprite.hpp"

struct RenderableAssignedSubscriber : public ECS::EventSubscriber<ECS::Events::OnComponentAssigned<Sprite>>
{
	typedef std::function<void(ECS::ComponentHandle<Sprite>)> RenderableAssignedCallback;

	virtual void receive(class ECS::World *world, const ECS::Events::OnComponentAssigned<Sprite> &event) override
	{
		auto renderable = event.entity->get<Sprite>();
		if (renderable && callback)
			callback(renderable);
	}

	RenderableAssignedCallback callback;
};
