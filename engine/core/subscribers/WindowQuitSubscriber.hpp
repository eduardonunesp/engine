#pragma once

#include <functional>
#include "../../ECS.hpp"
#include "../../events/WindowQuit.hpp"

struct WindowQuitSubscriber : public ECS::EventSubscriber<WindowQuit>
{
	typedef std::function<void()> WindowQuitCallback;

	virtual void receive(class ECS::World *world,
						 const WindowQuit &event) override
	{
		callback();
	}

	WindowQuitCallback callback;
};
