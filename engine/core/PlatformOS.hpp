#pragma once

class PlatformOS {
public:
	enum class Platform {
		UNKNOW = 0x0,
		MACOS = 0x1,
		IOS_SIM = 0x2,
		IOS_IPHONE = 0x3,
		WIN_386 = 0x4,
		WIN_X64 = 0x5,
		LINUX = 0x6
	};

	static Platform curr_platform_os;
};