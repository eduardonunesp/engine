#pragma once

#include <cassert>
#include "../ECS.hpp"

class SystemsRegistry {
public:
	void init(ECS::World* world)
	{
		assert(world != nullptr);
		this->world = world;
	}

	void add(ECS::EntitySystem* system)
	{
		assert(system != nullptr && "System is null");
		world->registerSystem(system);
	}

private:
	ECS::World *world;
};
