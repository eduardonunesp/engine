#include <cassert>
#include "SDLWrappers.hpp"
#include "../utils/Log.hpp"
#include "PlatformOS.hpp"

void Window::cleanup()
{
	if (window != nullptr) {
		DBG("Cleanup window memory");

		for (auto& texture : textures) {
			DBG("Cleanup texture memory " << texture.first);
			// texture.second.cleanup();
		}

		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
		SDL_Quit();
		window = nullptr;
	}
}

void Window::init(const std::string &win_title, const glm::vec2 &viewPort, const glm::vec2 &winPos)
{

#ifdef _DEBUG
	SDL_version compiled;
	SDL_version linked;

	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);
	SDL_Log("We compiled against SDL version %u.%u.%u ...\n",
		   compiled.major, compiled.minor, compiled.patch);
	SDL_Log("But we are linking against SDL version %u.%u.%u.\n",
		   linked.major, linked.minor, linked.patch);
#endif

	int r_drivers = SDL_GetNumRenderDrivers();
	DBG("RENDER DRIVERS: " << r_drivers);

	std::string prefered_driver = "opengl";

	switch (PlatformOS::curr_platform_os) {
		case PlatformOS::Platform::WIN_386:
		case PlatformOS::Platform::WIN_X64:
		prefered_driver = "direct3d";
		break;
		case PlatformOS::Platform::MACOS:
		prefered_driver = "metal";
		break;
		case PlatformOS::Platform::LINUX:
		prefered_driver = "opengl";
		break;
		default:
		prefered_driver = "opengl";
		break;
	}

	for (int i = 0; i < r_drivers; i++) {
		SDL_RendererInfo renderer_info;
		SDL_GetRenderDriverInfo(i, &renderer_info);
		DBG("Renderer driver available: " << renderer_info.name);
	}

	int v_drivers = SDL_GetNumVideoDrivers();
	DBG("VIDEO DRIVERS: " << v_drivers);

	for (int i = 0; i < v_drivers; i++) {
		DBG("Video driver available: " << SDL_GetVideoDriver(i));
	}

	DBG("Using render driver: " << prefered_driver);

	auto init_result = SDL_Init(SDL_INIT_VIDEO);
	assert(init_result == 0 && "Video system failed to initialize");

	auto window_flags = SDL_WINDOW_SHOWN;
	DBG("Creating window with title: " << win_title << " at " << winPos.x << "," << winPos.y << " with size " << viewPort.x << "," << viewPort.y);
	window = SDL_CreateWindow(win_title.c_str(), winPos.x, winPos.y, viewPort.x, viewPort.y, window_flags);
	assert(window && "Window failed to initialize");

	int renderer_flags = SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED;
	renderer = SDL_CreateRenderer(window, -1, renderer_flags);
	assert(renderer != nullptr && "Renderer failed to initialize");

#ifdef _DEBUG
	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	DBG("Window size: " << w << "x" << h);

	int display_count = SDL_GetNumVideoDisplays() - 1;
	DBG("Display count: " << display_count);

	int display_mode_count = SDL_GetNumDisplayModes(0);
	DBG("Display mode count: " << display_mode_count);

	SDL_DisplayMode mode;
	std::uint32_t f;

	for (int i = 0; i < display_mode_count; ++i) {
		if (SDL_GetDisplayMode(0, i, &mode) != 0) {
			ERR("SDL_GetDisplayMode failed: " << SDL_GetError());
			break;
		}

		f = mode.format;

		SDL_Log("Mode %i\tbpp %i\t%s\t%i x %i Hz %i", i,
			SDL_BITSPERPIXEL(f), SDL_GetPixelFormatName(f), mode.w, mode.h, mode.refresh_rate);
	}

	SDL_DisplayMode curr_mode;
	SDL_GetCurrentDisplayMode(display_count, &curr_mode);
	SDL_Log("Current Display Mode %i\tbpp %i\t%s\t%i x %i Hz %i", display_count,
		SDL_BITSPERPIXEL(f), SDL_GetPixelFormatName(f), curr_mode.w, curr_mode.h, curr_mode.refresh_rate);
	refresh_rate = curr_mode.refresh_rate;

	SDL_DisplayMode curr_desktop_mode;
	SDL_GetDesktopDisplayMode(display_count, &curr_desktop_mode);
	SDL_Log("Current Desktop Mode %i\tbpp %i\t%s\t%i x %i Hz %i", display_count,
		SDL_BITSPERPIXEL(f), SDL_GetPixelFormatName(f), curr_desktop_mode.w, curr_desktop_mode.h, curr_desktop_mode.refresh_rate);

	DBG("VideoManager Ready " + std::to_string(w) + ":" + std::to_string(h));
	DBG("Refresh rate: " << refresh_rate);

	SDL_RendererInfo renderer_info;
	SDL_GetRendererInfo(renderer, &renderer_info);
	DBG("Renderer name: " << renderer_info.name);
	DBG("Renderer max texture width: " << renderer_info.max_texture_width);
	DBG("Renderer max texture height: " << renderer_info.max_texture_height);
#endif
}

const Texture* Window::load_texture(const std::string &path)
{
	if (textures.find(path) != textures.end()) {
		return textures[path];
	}

	auto sdl_surface = IMG_Load(path.c_str());
	assert(sdl_surface != nullptr && "Failed to load image");

	auto texture = new Texture();
	texture->sdl_texture = SDL_CreateTextureFromSurface(renderer, sdl_surface);
	assert(texture->sdl_texture != nullptr && "Failed to create texture");
	SDL_FreeSurface(sdl_surface);

	textures[path] = texture;

	DBG("Loaded texture: " << path);
	return textures[path];
}

void Window::draw(const std::string& texture_id, const Rect &src, const Rect &dst) {
	auto& texture = textures[texture_id];

	texture->draw(*this, src, dst);
}

void Texture::draw(const Window &window, const Rect &src, const Rect &dst)
{
	if (sdl_texture == nullptr)
		return;

	SDL_Rect src_rect = rect_to_sdl_rect(src);
	SDL_Rect dst_rect = rect_to_sdl_rect(dst);
	SDL_RenderCopy(window.get_renderer(), sdl_texture, &src_rect, &dst_rect);
}