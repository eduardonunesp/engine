#pragma once

#include <map>
#include <string>
#include "../ECS.hpp"
#include "../events/LoadMap.hpp"
#include "subscribers/AppReadySubscriber.hpp"
#include "tilemap/Map.hpp"

class MapsRegistry {
public:
	typedef std::map<std::string, Map*> Maps;

	void init(ECS::World* world)
	{
		assert(world != nullptr);
		this->world = world;

		app_ready_subscriber.callback = [&](AppReady event) {
			for (auto& map : maps) {
				map.second->set_window(event.window);
			}
		};
		world->subscribe<AppReady>(&app_ready_subscriber);
	}

	inline void add(const std::string& map_name, Map* map) {
		assert(map != nullptr && "Map cannot be nullptr");
		maps[map_name] = map;
		world->emit(LoadMap{ map });
	}

	inline void render() {

		// for (auto& map : maps) {
			// map.second->render();
		// }
	}

private:
	ECS::World *world;

	AppReadySubscriber app_ready_subscriber;

	Maps maps;
};
